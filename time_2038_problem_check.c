#include <stdio.h>
#include <time.h>

/*
 * The result should look something like
 * Fri 2008-08-22 15:21:59 WAST
 */


void showtime(unsigned long epoch_num) {
    time_t     now;
    struct tm *ts;
    char       buf[80];

    /* Get the current time */
    now = epoch_num;
	  printf("; epoch_num = %lu\n", now);


    /* Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz" */
    ts = localtime(&now);
    strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", ts);
    puts(buf);
    ts = gmtime(&now);
    strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", ts);
    puts(buf);
}


int main(void)
{

    /* show the current time */
    printf("---------- current time");
    showtime(time(NULL));

    /* show Year 2038 problem */
    printf("---------- before 2038 problem");
    showtime(0x8FFFFFFF);
    printf("---------- after 2038 problem");
    showtime(0xFFFFFFFF);


    return 0;
}
